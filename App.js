import React, { Component } from 'react'
import { AppRegistry, Switch } from 'react-native'
import {
  Container,
  Root,
	Content,
	Body,
	List,
	ListItem,
	Right,
	Left,
	Tab,
	Tabs,
	Text,
	Button,
	Form,
  Item,
  Toast,
	Input
} from 'native-base'
import Icon from 'react-native-vector-icons/AntDesign'
import _ from 'lodash'
import BluetoothSerial from 'react-native-bluetooth-serial'
import { removeData, retrieveItems, storeData } from './helpers/storageHelper'
import { sendSMSFunction } from './helpers/smsHelper'

const initialState = {
	name: '',
	contactNumber: ''
}

export default class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			...initialState,
			values: [],
			isEnabled: false,
			discovering: false,
			devices: [],
			unpairedDevices: [],
      connected: false
		}
	}

	componentDidMount() {
    retrieveItems(this.setState.bind(this))
    this.readData()
	}

	componentWillMount() {
		Promise.all([BluetoothSerial.isEnabled(), BluetoothSerial.list()]).then(values => {
			const [isEnabled, devices] = values
      this.setState({ isEnabled, devices })
		})
		BluetoothSerial.on('bluetoothEnabled', () => {
			Promise.all([BluetoothSerial.isEnabled(), BluetoothSerial.list()]).then(values => {
				const [isEnabled, devices] = values
        this.setState({ isEnabled, devices })
			})
			BluetoothSerial.on('bluetoothDisabled', () => {
				this.setState({ devices: [] })
			})
			BluetoothSerial.on('error', err => console.log(`Error: ${err.message}`))
		})
	}

	async discoverAvailableDevices() {
		if (this.state.discovering) {
			return false
		} else {
			this.setState({ discovering: true })
			try {
				const unpairedDevices = await BluetoothSerial.discoverUnpairedDevices()
				const uniqueDevices = _.uniqBy(unpairedDevices, 'id')
				this.setState({ unpairedDevices: uniqueDevices, discovering: false })
			} catch (e) {
				console.log(e)
			}
		}
	}

	async enable() {
		try {
			await BluetoothSerial.enable()
			this.setState({ isEnabled: true })
		} catch (e) {
			console.log(e)
		}
	}

	async disable() {
		try {
			await BluetoothSerial.disable()
			this.setState({ isEnabled: false })
		} catch (e) {}
	}

	toggleBluetooth(value) {
		if (value === true) {
			this.enable()
		} else {
			this.disable()
		}
	}

  connect (device) {
    this.setState({ connecting: true })
    BluetoothSerial.connect(device.id)
    .then((res) => {
      console.log(`Connected to device ${device.name}`);
      Toast.show({text: `Connected to device ${device.name}`, duration: 1500});
    })
    .catch((err) => {
      Toast.show({text: err, duration: 1500})
    })
  }

	renderItem(list) {
		return list.map((obj, id) => {
			return (
				<ListItem key={id}>
					<Body>
						<Text>{obj.name}</Text>
						<Text note>{obj.contactNumber}</Text>
					</Body>
					<Button
						transparent
						onPress={() => {
							removeData(obj.contactNumber)
							retrieveItems(this.setState.bind(this))
						}}>
						<Icon name="delete" size={20} />
					</Button>
				</ListItem>
			)
		})
	}

	renderDevicesList(list) {
		return (
			<Content>
				<ListItem itemDivider>
					<Body>
						<Text>Available Devices</Text>
					</Body>
				</ListItem>
				{list.map((obj, id) => {
					return (
						<ListItem key={id} onPress={() => this.connect(obj)}>
							<Body>
								<Text>{obj.name}</Text>
							</Body>
						</ListItem>
					)
				})}
			</Content>
		)
	}

  readData() {
    console.log('start')
    setInterval(() => {
      BluetoothSerial.readFromDevice().then((data) => {
        if(data.includes('send')) {
          sendSMSFunction(this.state.values)
        }
      })
    }, 2000)
  }

	render() {
    console.log(this.state)
		return (
      <Root>
			<Container>
				<Tabs>
					<Tab heading="Bluetooth">
						<Content>
							<ListItem>
								<Left>
									<Text>Bluetooth {this.state.isEnabled ? 'enabled' : 'disabled'}</Text>
								</Left>
								<Right>
									<Switch
										value={this.state.isEnabled}
										onValueChange={val => this.toggleBluetooth(val)}
									/>
								</Right>
							</ListItem>
							<ListItem>
								<Left>
									<Button
										transparent
										rounded
										full
										onPress={this.discoverAvailableDevices.bind(this)}>
										<Text>Scan for devices</Text>
									</Button>
								</Left>
								<Right>
									<Text>{this.state.discovering ? 'Scanning' : 'Not Scanning'}</Text>
								</Right>
							</ListItem>
							{this.renderDevicesList(this.state.unpairedDevices)}
						</Content>
					</Tab>
					<Tab heading="Contacts">
						<Content>
							<List>{this.renderItem(this.state.values)}</List>
						</Content>
					</Tab>
					<Tab heading="Add New Contact">
						<Content>
							<Form>
								<Item>
									<Input
										onChangeText={e => this.setState({ name: e })}
										value={this.state.name}
										placeholder={'Name'}
									/>
								</Item>
								<Item last>
									<Input
										onChangeText={e => this.setState({ contactNumber: e })}
										value={this.state.contactNumber}
										keyboardType={'phone-pad'}
										placeholder={'Contact Number'}
										maxLength={11}
									/>
								</Item>
							</Form>
							<Button
								full
								rounded
								style={{ marginTop: 30, width: '70%', alignSelf: 'center' }}
								onPress={() => {
									if (this.state.contactNumber.length === 11) {
										storeData(this.state)
										this.setState(initialState)
									}
									retrieveItems(this.setState.bind(this))
								}}>
								<Text>CONFIRM</Text>
							</Button>
						</Content>
					</Tab>
				</Tabs>
			</Container>
      </Root>
		)
	}
}

AppRegistry.registerComponent('App', () => App)
