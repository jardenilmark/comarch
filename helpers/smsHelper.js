import { Toast } from 'native-base';
import SendSMS from 'react-native-sms-x';

export const sendSMSFunction = list => {
  navigator.geolocation.getCurrentPosition(position => {
    const { latitude, longitude } = position.coords
    list.forEach((e, id) => {
      SendSMS.send(id, `${e.contactNumber}`,
        `I am in trouble ${e.name}!\nCoordinates:\nlatitude: ${latitude}\nlongitude: ${longitude}\ntimestamp: ${new Date}`,
        (id, msg)=>{
          Toast.show({ text: msg, duration: 1500 })
      })
    })
  }, error => { 
    Toast.show({ text: 'Turn on location', duration: 1500 }) 
    })
}