import { AsyncStorage } from 'react-native';

export const storeData = async (data) => {
  try {
    await AsyncStorage.setItem(data.contactNumber, JSON.stringify(data))
  } catch (e) {
    console.log(e)
  }
}
export const removeData = async (key) => {
  try {
    await AsyncStorage.removeItem(key)
  } catch (e) {
    console.log(e)
  }
}

export const retrieveItems = setState => {
  AsyncStorage.getAllKeys((err, keys) => {
    AsyncStorage.multiGet(keys, (err, stores) => {
      setState({ values: stores.map((result, i, store) => {
        let value = store[i][1]
        return JSON.parse(value)
      })});
    });
  });
}